// reading a text file
#include "data.h"
#include "ReadCRT.h"
#include "Schedule/smethods.h"
#include <string>
#include <sstream>
#include "Schedule/feasibletable.h"
#include "Schedule/improvetable.h"
#include "Schedule/enhancement.h"


using namespace std;

int main () {
	 srand(time(NULL));
	const char* filename = "InputData/ITC-2007_ectt/comp06.ectt";
	//const char* solfilename = "/home/patrick/work/UCTP-CPP/Results/comp01.sol";
	const char* solfilename = "/home/patrick/work/UCTP-CPP/Solutions/genKempe-26-comp06";
	//const char* solfilename = "/home/patrick/work/UCTP-CPP/Solutions/genKempe-7-comp04";
	//const char* solfilename = "/home/patrick/work/UCTP-CPP/tempResult.sol";
	//const char* filename = "InputData/Test_ectt/toy.ectt";
	Data* data = new Data(filename);
	

	cout<<"\nName: " << data->getName()<<endl;
	cout<<"Courses: " << data->getNumCourses()<<endl;
	cout<<"Rooms: " << data->getNumRooms()<<endl;
	cout<<"Days: " << data->getNumDays()<<endl;
	cout<<"Periods_per_day: " << data->getNumPeriodsPerDay()<<endl;
	cout<<"Curricula: " << data->getNumCurricula()<<endl;
	cout<<"Min_Max_Daily_Lectures: " << data->getMinDaily() << " " <<data->getMaxDaily()<<endl;
	cout<<"UnavailabilityConstraints: " << data->getNumUnavailabilityConstraint()<<endl;
	cout<<"RoomConstraint (Feature): " << data->getNumRoomConstraints()<<endl;

	int maximumPeriod = (data->getNumDays() * data->getNumPeriodsPerDay()) - 1;
	SM::numPeriods = maximumPeriod + 1;

	FeasibleTable ft(data->getCourses(),  data->getRooms(), maximumPeriod,  data->getNumPeriodsPerDay(), data->getCurriculum());
	//std::clock_t t1,t2;
	//ft.printAdjMat();


	cout<<endl<<endl;



	cout<<"Construction ...."<<endl;
    double wall0 =  SM::get_wall_time();
    double cpu0  = SM::get_cpu_time();
	//ft.antColonyThread(8, 500);


	auto fromOld = readCRT(data->getCourses(), data->getRooms(), data->getNumPeriodsPerDay(), solfilename);
  	ft.setFeasibleTable(fromOld);
  	


	cout<<endl<<endl<<"Improvement Phase..."<<endl;

    
    ImproveTable impTable(ft.getVenueTime(), data->getRooms(), ft.getCurCodes(), ft.getCourse(), ft.getFeasibleTable(), ft.getMaxPeriod(), ft.getPeriodsInDay());
    cout<<" Timetable has "<<ft.NumberHCV()<<" number of hard constraint violations and " <<impTable.NumberSCV(fromOld) <<" soft constraints" <<endl;
    //int max = ft.getFeasibleTable().size() - 1;
    //int min = 0;
    //int crtOneIndex = min + (rand() % (int)(max - min + 1));  //generate random number in range [min max]
    //int crtTwoIndex = min + (rand() % (int)(max - min + 1));  //generate random number in range [min max]
    //cout<<"Index One: "<<crtOneIndex<<" Index Two: "<<crtTwoIndex<<endl;
    //auto timet = impTable.singleSwap(60, 142, ft.getFeasibleTable());
    auto timet= fromOld;
    //auto timet = impTable.reassignRooms(fromOld);
    // timet = impTable.runImprovement(1,1,1);
    //fromOld = timet;
    ft.setFeasibleTable(timet);
    /*bool nType = true;
    for (int j =0; j < 3; j++)
    {
    	int prev = impTable.NumberSCV(ft.getFeasibleTable());
    	for (size_t i =0; i < ft.getFeasibleTable().size(); i++)
    	{
    		cout<<"Iteration "<<j<<" index: "<<i<<" ";
    		timet = impTable.bestNeighbour((int)i, fromOld, nType);
    		auto currentSCV = impTable.NumberSCV(timet);
    		std::cout<< currentSCV<<" is the current SCV"<<endl;
    		if (currentSCV < impTable.NumberSCV(fromOld))
    		{
    			//cout<<" I am here "<<impTable.NumberSCV(fromOld)<<endl;
    			fromOld = timet;
    			//timet = fromOld;
    		}
    	}
    	ft.setFeasibleTable(fromOld);
    	int current = impTable.NumberSCV(ft.getFeasibleTable());
    	if (current == prev)
    	{
    		if (nType == true)
    			nType = false;
    		else
    			nType = true; 
    	}
    	cout<<"number of violations in iteration "<<j << " is "<< current << "  "<<nType;
    }*/
    
    //vector<int> chromosome;
    //auto timet = impTable.applyChomosome(chromosome, ft.getFeasibleTable());
    //auto timet = impTable.bestNeighbour(10, ft.getFeasibleTable());
    Enhancement enc(ft);
    auto newT = enc.runEnhancement(1, 3, 1);
    //auto newT = ft.getFeasibleTable();
    std::stringstream ss;
    ss << "Solutions/" << data->getName();
    impTable.writeTimetableToFileSolutionFormat(ss.str(), newT);







    double wall6 =   SM::get_wall_time();
    double cpu6 = SM::get_cpu_time();
	std::cout <<endl<< "Full Timetable construction " <<  wall6 - wall0 <<" wall seconds and "<<  cpu6 - cpu0 <<" CPU seconds"<<endl;


	int roomStabilityViolations =   0;
	int courseWorkingDayViolations =   0;
	int consecutiveLectureViolations =   0;
	int sizeViolations = 0;

	cout<<endl<<endl<<"***********************Improved Timetable2*****************"<<endl;
	cout<<endl<<endl<<" New Timetable has "<<ft.NumberHCV(newT)<<" number of hard constraint violations"<<endl;
	cout<<" New Timetable has "<<impTable.NumberSCV(newT,&roomStabilityViolations, &courseWorkingDayViolations, &consecutiveLectureViolations, &sizeViolations)<<" number of soft constraint violations"<<endl;
	cout<<"Room Size Violations: "<<sizeViolations<<" Room Stability: " << roomStabilityViolations << " Course Working Day Violations: " << courseWorkingDayViolations << " Consecutive Lecture Violations: " << consecutiveLectureViolations<<endl;

	cout<<endl<<"**************Old Timetable**********************"<<endl;
	cout<<" Timetable has "<<ft.NumberHCV()<<" number of hard constraint violations"<<endl;
	cout<<" Old Timetable has "<<impTable.NumberSCV(ft.getFeasibleTable(),&roomStabilityViolations, &courseWorkingDayViolations, &consecutiveLectureViolations, &sizeViolations)<<" number of soft constraint violations"<<endl;
	cout<<"Room Size Violations: "<<sizeViolations<<" Room Stability: " << roomStabilityViolations << " Course Working Day Violations: " << courseWorkingDayViolations << " Consecutive Lecture Violations: " << consecutiveLectureViolations<<endl;

	cout<<"Threaded HCV: "<<SM::ThreadNumberHCV(newT, (maximumPeriod + 1));
 	impTable.writeTimetableToFile("ImproveTab.csv");



 	/*
	CourseRoomTime crt = impTable.voteOut(0, ft.getFeasibleTable());
	auto invitations = impTable.invites(crt, ft.getFeasibleTable());
	auto invited = impTable.inviteMinIn(invitations, ft.getFeasibleTable());
	cout<<endl<<endl<<"Voted out "<<crt.toString()<<" best invite is "<<invited<<endl;
	//impTable.writeTimetableToFile2();
	auto newT = impTable.applyInvite(invited, crt, ft.getFeasibleTable());
	cout<<endl<<endl<<" New Timetable has "<<ft.NumberHCV(newT)<<" number of hard constraint violations"<<endl;
	cout<<" New Timetable has "<<impTable.NumberSCV(newT)<<" number of hard constraint violations"<<endl;
	*/

	impTable.writeTimetableToFile2();


	delete data;
	//cout<<"Maximum period is: "<<maximumPeriod;

	return 0;
}
